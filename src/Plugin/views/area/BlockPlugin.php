<?php

namespace Drupal\vabp\Plugin\views\area;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\Plugin\Block\Broken;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area handler block plugin.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("vabp")
 */
class BlockPlugin extends AreaPluginBase {

  /**
   * The block manager.
   * 
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected BlockManagerInterface $blockManager;

  /**
   * The current user.
   * 
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * 
   * @param array $configuration 
   *  The configuration.
   * @param mixed $plugin_id
   *  The ID of the plugin being instantiated.
   * @param mixed $plugin_definition
   *  The definition of the plugin being instantiated.
   * @param BlockManagerInterface $block_manager
   *  The block plugin manager.
   * @param AccountInterface $current_user
   *  The current user.
   * @return void 
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BlockManagerInterface $block_manager, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->blockManager = $block_manager;
    $this->currentUser = $current_user;
}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.block'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['plugin'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['plugin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Plugin ID'),
      '#default_value' => $this->options['plugin'],
      '#description' => $this->t('Provide a block plugin ID.'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    $plugin_id = $form_state->getValue(['options', 'plugin']);
    $plugin = $this->blockManager->createInstance($plugin_id, []);

    // The block manager returns a fallback plugin
    // so there's no point in catching a PluginException
    // in this case. Instead, we can simply check if
    // $plugin is the fallback plugin.
    if ($plugin instanceof Broken || $plugin == NULL) {
      $form_state->setError($form['plugin'], $this->t("A plugin with the specified ID ({$plugin_id}) doesn't exist"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(array $results) {
    parent::preRender($results);
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    // You can hard code configuration or you load from settings.
    $config = [];

    /** @var \Drupal\Core\Block\BlockPluginInterface $plugin_block */
    $plugin_block = $this->blockManager->createInstance($this->options['plugin'], $config);

    // Some blocks might implement access check.
    $access_result = $plugin_block->access($this->currentUser);

    // $access_result can be boolean or an AccessResult class
    if (is_object($access_result) && $access_result->isForbidden() || is_bool($access_result) && !$access_result) {
      // We might need to add some cache tags/contexts.
      return [];
    }

    $render['content'] = $plugin_block->build();

    if (!empty($render['content'])) {
      $render += [
        '#theme' => 'block',
        '#id' => $this->options['plugin'],
        '#attributes' => [],
        '#contextual_links' => [],
        '#configuration' => $plugin_block->getConfiguration(),
        '#plugin_id' => $plugin_block->getPluginId(),
        '#base_plugin_id' => $plugin_block->getBaseId(),
        '#derivative_plugin_id' => $plugin_block->getDerivativeId(),
      ];
    }

    // In some cases, there might be a need for cache tags/context depending on
    // the block implemention. As it's possible to add the cache tags and
    // contexts in the render method and in ::getCacheTags and
    // ::getCacheContexts methods.
    return $render;
  }

}
