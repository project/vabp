<?php

namespace Drupal\Tests\vabp\Unit\Plugin\area;

use Drupal\block\BlockInterface;
use Prophecy\Argument;
use Drupal\views\ViewsData;
use Drupal\views\Entity\View;
use Drupal\Tests\UnitTestCase;
use Drupal\Core\Block\BlockBase;
use Drupal\views\ViewExecutable;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\Form\FormState;
use Drupal\Core\Session\AccountInterface;
use Drupal\vabp\Plugin\views\area\BlockPlugin;
use Drupal\Core\Routing\RouteProviderInterface;
use Prophecy\Prophecy\ObjectProphecy;
use Prophecy\Exception\Doubler\ClassNotFoundException;
use Prophecy\Exception\Doubler\DoubleException;
use Prophecy\Exception\Doubler\InterfaceNotFoundException;
use Prophecy\Exception\Prophecy\ObjectProphecyException;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @coversDefaultClass \Drupal\vabp\Plugin\views\area\BlockPlugin
 * @group vabp
 */
class BlockPluginTest extends UnitTestCase {

  /**
   * The tested entity area handler.
   *
   * @var \Drupal\vabp\Plugin\views\area\BlockPlugin
   */
  protected $blockPluginHandler;

  /**
   * The block manager.
   * 
   * @var \Drupal\Core\Block\BlockManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $blockManager;

  /**
   * The view executable object.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * 
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $storage = $this->prophesize(View::class);
    $storage->label()->willReturn('ResultTest');
    $storage->set(Argument::cetera())->willReturn(NULL);

    $views_data = $this->prophesize(ViewsData::class)->reveal();
    $route_provider = $this->prophesize(RouteProviderInterface::class)->reveal();

    $this->user = $this->prophesize(AccountInterface::class)->reveal();
    $this->view = new ViewExecutable($storage->reveal(), $this->user, $views_data, $route_provider);

    $block = $this->createTestBlock('test');
    $block->access($this->user)->willReturn(TRUE);

    $this->createHandler($block->reveal(), $this->user);
  }

  /**
   * Tests the render method.
   *
   * @covers ::render
   */
  public function testRender() {
    // The handler is configured to show with empty views by default, so should
    // appear.
    $this->assertSame(['test' => 'test'], $this->blockPluginHandler->render());

    // We want to test access on the block as well.
    $block = $this->createTestBlock('test');
    $block->access($this->user)->willReturn(FALSE);

    $this->createHandler($block->reveal(), $this->user);
    $this->assertSame([], $this->blockPluginHandler->render());
  }

  /**
   * Tests the validation of the options form.
   *
   * @covers ::validateOptionsForm
   */
  public function testValidation() {
    $form = [];
    $form_state = new FormState();

    $this->blockPluginHandler->setStringTranslation($this->getStringTranslationStub());

    $form_state->setValue(['options', 'plugin'], 'test');

    $this->blockPluginHandler->validateOptionsForm($form, $form_state);
    $this->assertEmpty($form_state->getErrors());
  }

  /**
   * 
   * @param mixed $block 
   * @param mixed $user 
   * @return void 
   */
  private function createHandler($block, $user) {
    $this->blockPluginHandler = new BlockPlugin([], 'result', [], $this->blockManager, $user);
    $this->blockPluginHandler->options = ['plugin' => $block->getPluginId(), 'admin_label' => 'test'];
    $this->blockPluginHandler->view = $this->view;
  }

  /**
   * 
   * @return ObjectProphecy 
   * @throws ClassNotFoundException 
   * @throws DoubleException 
   * @throws InterfaceNotFoundException 
   * @throws ObjectProphecyException 
   */
  private function createTestBlock(string $id) {
    $block = $this->prophesize(BlockBase::class);
    $block->build()->willReturn(['test' => 'test']);
    $block->getPluginId()->willReturn($id);

    $block_manager = $this->prophesize(BlockManager::class);
    $block_manager->createInstance($id, [])->willReturn($block->reveal());
    $this->blockManager = $block_manager->reveal();
    
    return $block;
  }

}
