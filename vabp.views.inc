<?php

/**
 * @file
 * Provide views data
 */

 /**
 * Implements hook_views_data().
 */
function vabp_views_data() {
    $data['views']['vabp'] = [
      'title' => t('Render block plugin'),
      'help' => t('Provides a handler to render block plugins.'),
      'area' => [
        'id' => 'vabp',
      ],
    ];
  
    return $data;
}
